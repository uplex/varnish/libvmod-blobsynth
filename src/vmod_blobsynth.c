#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cache/cache.h"
#include "vcl.h"
#include "vsb.h"

#include "vtim.h"
#include "vcc_blobsynth_if.h"

VCL_VOID
vmod_synthetic(VRT_CTX, VCL_BLOB body)
{
	struct vsb *vsb;

	AN(ctx->method & (VCL_MET_SYNTH | VCL_MET_BACKEND_ERROR));

	CAST_OBJ_NOTNULL(vsb, ctx->specific, VSB_MAGIC);

	if (body == NULL || body->len == 0)
		return;
	AN(body->blob);
	if (VSB_bcat(vsb, body->blob, body->len))
		VRT_fail(ctx, "blobsynth.synthetic(): %s",
		strerror(VSB_error(vsb)));
}
